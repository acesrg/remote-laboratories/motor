/*
 * Copyright 2020 Marco Miretti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file encoder.c */

/* third party libs */
#include <esp8266.h>
#include <FreeRTOS.h>
#include <task.h>

/* third party local libs */
#include <log.h>

/* local libs */
#include <encoder.h>

/**
 * \brief   A quadrature encoder structure.
 */
typedef struct EncoderObjectType {
    uint8_t channel_a;  /**< \brief The encoder channel a. */
    uint8_t channel_b;  /**< \brief The encoder channel b. */
    size_t  last_state; /**< \brief A latch of the last encoder state. */
    int64_t value;     /**< \brief The output value. */
    bool initialized;   /**< \brief Singleton, indicates if already initialized. */
} EncoderObjectType;

/**
 * \brief   The possible events associated with and encoder interrupt.
 *
 * RISE and FALL refer to the electrical rising and falling edges.
 */
typedef enum EncoderEventType {
    NO_MEASURE,
    CHANNEL_A_RISE,
    CHANNEL_B_RISE,
    CHANNEL_A_FALL,
    CHANNEL_B_FALL
} EncoderEventType;

/**< \brief This subtraction means an encoder increment */
#define INCREMENT_1 (uint8_t) (CHANNEL_A_RISE - CHANNEL_B_FALL)
/**< \brief This subtraction means an encoder increment */
#define INCREMENT_2 (uint8_t) (CHANNEL_B_RISE - CHANNEL_A_RISE)
/**< \brief This subtraction means an encoder increment */
#define INCREMENT_3 (uint8_t) (CHANNEL_A_FALL - CHANNEL_B_RISE)
/**< \brief This subtraction means an encoder increment */
#define INCREMENT_4 (uint8_t) (CHANNEL_B_FALL - CHANNEL_A_FALL)

/**< \brief This subtraction means an encoder decrement */
#define DECREMENT_1 (uint8_t) (CHANNEL_B_RISE - CHANNEL_A_FALL)
/**< \brief This subtraction means an encoder decrement */
#define DECREMENT_2 (uint8_t) (CHANNEL_A_RISE - CHANNEL_B_RISE)
/**< \brief This subtraction means an encoder decrement */
#define DECREMENT_3 (uint8_t) (CHANNEL_B_FALL - CHANNEL_A_RISE)
/**< \brief This subtraction means an encoder decrement */
#define DECREMENT_4 (uint8_t) (CHANNEL_A_FALL - CHANNEL_B_FALL)

/**< \brief Encoder resolution */
#define ENCODER_RESOLUTION 200
/**< \brief milliseconds to seconds conversion */
#define MS_TO_S 1000
/**< \brief seconds to minutes conversion */
#define S_TO_MIN 60

/**
 * \brief	Global encoder object to be used by the ISR.
 */
EncoderObjectType encoder = {.channel_a = 0,
                             .channel_b = 0,
                             .last_state = NO_MEASURE,
                             .value = 0,
                             .initialized = false};

void encoder_isr_handler(uint8_t gpio_num);

int64_t get_encoder_value() {
    return encoder.value;
}

float get_encoder_rate() {
    static TickType_t lastTick = 0;
    static int64_t lastValue = 0;

    TickType_t currentTick = xTaskGetTickCount();
    int64_t currentValue = encoder.value;

    float rate = ((float) (currentValue - lastValue) / ENCODER_RESOLUTION) /
                 ((float) (currentTick - lastTick) * portTICK_PERIOD_MS / (MS_TO_S * S_TO_MIN));
    lastValue = currentValue;
    lastTick = currentTick;

    return rate;
}

void set_encoder_value(int64_t value) {
    encoder.value = value;
}

void quadrature_encoder_init(uint8_t channel_a, uint8_t channel_b) {
    if (!encoder.initialized) {
        encoder.initialized = true;
        log_info("Init quadrature encoder");
        log_info("Set gpio as input");
        gpio_enable(channel_a, GPIO_INPUT);
        gpio_enable(channel_b, GPIO_INPUT);

        encoder.channel_a = channel_a;
        encoder.channel_b = channel_b;

        encoder.value = 0;

        gpio_set_interrupt(channel_a, GPIO_INTTYPE_EDGE_ANY, encoder_isr_handler);
        gpio_set_interrupt(channel_b, GPIO_INTTYPE_EDGE_ANY, encoder_isr_handler);
    } else {
        log_info("Encoder was already initialized");
    }
}


/**
 * \brief   Detects the nature of the encoder interrupt
 * \note    Given the pin number and the state of said
 *          pin, it can define if the interruption was
 *          caused by a rising or falling edge on either
 *          channel_a or channel_b on the encoder.
 *
 * \param   gpio_num: the pin number
 * \return  EncoderEventType: the type of said
 *          interrupt (rising, falling and which pin)
 **/
EncoderEventType detect_event_nature(uint8_t gpio_num) {
    if (gpio_num == encoder.channel_a) {
        return gpio_read(gpio_num) ? CHANNEL_A_RISE : CHANNEL_A_FALL;
    } else if (gpio_num == encoder.channel_b) {
        return gpio_read(gpio_num) ? CHANNEL_B_RISE : CHANNEL_B_FALL;
    } else {
        return NO_MEASURE;
    }
}

/**
 * \brief quadrature encoder interrupt service routine
 * \note    According to the event nature decides if the
 *      counter should be incremented, decremented or
 *      kept as is.
 *
 * \param gpio_num: the pin number
 **/

void encoder_isr_handler(uint8_t gpio_num) {
    EncoderEventType current_event_nature = detect_event_nature(gpio_num);

    if (encoder.last_state != NO_MEASURE) {
        uint8_t diff = current_event_nature - encoder.last_state;
        if (diff == INCREMENT_1 || diff == INCREMENT_2 || diff == INCREMENT_3 || diff == INCREMENT_4)
            encoder.value++;
        else if (diff == DECREMENT_1 || diff == DECREMENT_2 || diff == DECREMENT_3 || diff == DECREMENT_4)
            encoder.value--;
    }
    encoder.last_state = current_event_nature;
}
