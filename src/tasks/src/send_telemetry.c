/*
 * Copyright 2022 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file send_telemetry.c */
/* standard */
#include <string.h>

/* third party libs */
#include <FreeRTOS.h>
#include <semphr.h>
#include <task.h>
#include <httpd/httpd.h>
#include <tcp.h>
#include <time.h>

/* third party local libs */
#include <log.h>

/* local libs */
#include <encoder.h>
#include <motor.h>
#include <json_parser.h>

/* project tasks */
#include <send_telemetry.h>

/* configuration includes */
#include <pinout_configuration.h>

/** \brief The period in which telemetry will be read and sent to the websocket. */
uint16_t TELEMETRY_PERIOD_ms = DEFAULT_TELEMETRY_PERIOD_ms;

static simple_json_t sensor_db[5] = {{.name = "timestamp", .type = DOUBLE, .value.double_type = 0.0},
                                     {.name = "count", .type = INT64, .value.int64_type = 0},
                                     {.name = "rate", .type = FLOAT, .value.float_type = 0.0},
                                     {.name = "duty", .type = INT32, .value.int32_type = 0},
                                     {.name = "error", .type = UINT8, .value.uint8_type = 0}};

/**
 * \brief Initializes encoder, then sends telemetry in a loop.
 *
 * \param *pvParameter: Basically, the TCP socket structure.
 */
void send_telemetry_task(void *pvParameter) {
    log_trace("task started");
    extern TaskHandle_t xHandle2;
    struct tcp_pcb *pcb = (struct tcp_pcb *) pvParameter;
    struct timeval tp;

    /** \todo The encoder initialization should be an HTTP endpoint, out of here. */
    log_trace("encoder init");
    quadrature_encoder_init(ENCODER_A, ENCODER_B);

    // Initialise the xLastWakeTime variable with the current time.
    TickType_t xLastWakeTime = xTaskGetTickCount();
    while (1) {
        gettimeofday(&tp, NULL);
        sensor_db[0].value.double_type = (double) tp.tv_sec + (double) tp.tv_usec / US_TO_S;
        sensor_db[1].value.int64_type = get_encoder_value();
        sensor_db[2].value.float_type = get_encoder_rate();
        // Agregar mutex
        sensor_db[3].value.int32_type = get_motor_duty_value();

        log_trace("timestamp = %.3f, count = %ld, rate = %.2f, duty = %ld",
                                                                      sensor_db[0].value.double_type,
                                                                      (int32_t) sensor_db[1].value.int64_type,
                                                                      sensor_db[2].value.float_type,
                                                                      (int32_t) sensor_db[3].value.int32_type);

        char composed_json[JSON_SENSOR_MAX_LEN];
        size_t database_size = sizeof(sensor_db)/sizeof(*sensor_db);

        retval_t compose_rv = json_simple_compose(composed_json, sensor_db, database_size);
        if (compose_rv != RV_OK) {
            log_error("compose error");
        }

        LOCK_TCPIP_CORE();
        websocket_write(pcb, (uint8_t *) composed_json, strlen(composed_json), WS_TEXT_MODE);

        // send immediately everything written to TCP socket
        tcp_output(pcb);
        UNLOCK_TCPIP_CORE();

        vTaskDelayUntil(&xLastWakeTime, TELEMETRY_PERIOD_ms / portTICK_PERIOD_MS);
        if (pcb == NULL || pcb->state != ESTABLISHED) {
            log_info("disconected, delete task");
            vTaskDelete(xHandle2);
        }
    }
}
