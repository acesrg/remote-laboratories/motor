/*
 * Copyright 2022 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file update_motor.h */
#ifndef SRC_TASKS_INC_UPDATE_MOTOR_H_
#define SRC_TASKS_INC_UPDATE_MOTOR_H_

/*
 * Telemetry definitions
 * */
#define MAX_MOTOR_PERIOD_ms             60000   /**< \brief Arbitrary max motor refresh period */
#define DEFAULT_MOTOR_UPDATE_PERIOD_ms  50      /**< \brief Comfortable motor refresh period */
#define MIN_MOTOR_PERIOD_ms             5       /**< \brief Min refresh period (hw limit) */

void update_motor_task(void *pvParameter);

#endif /* SRC_TASKS_INC_UPDATE_MOTOR_H_ */

