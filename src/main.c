/*
 * Copyright 2021 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file main.c */
/* standard */
#include <string.h>

/* third party libs */
#include <FreeRTOS.h>
#include <task.h>
#include <espressif/esp_common.h>
#include <espressif/user_interface.h>
#include <esp/uart.h>
#include <httpd/httpd.h>
#include <semphr.h>
#include <sntp.h>
#include <time.h>

/* third party local libs */
#include <log.h>
#include <ssi_utils.h>

/* local tasks */
#include <send_telemetry.h>
#include <update_motor.h>
#include <sample_task.h>

/* configuration includes */
#include <pinout_configuration.h>
#include <private_ssid_config.h>

/* proyect callbacks/cgi */
#include <logger_cgi.h>
#include <encoder_cgi.h>
#include <motor_cgi.h>
#include <telemetry_callback.h>
#include <telemetry_cgi.h>

#define DEFAULT_UART_NUMBER         0                               /**< \brief Default number for UART hardware. */
#define DEFAULT_BAUD_RATE           115200                          /**< \brief Default baud rate for UART. */
#define HTTP_DELAY_MS               10000                           /**< \brief Delay for HTTP task in ms. */
#define HTTP_DELAY                  pdMS_TO_TICKS(HTTP_DELAY_MS)    /**< \brief Delay for HTTP task in ticks. */
#define HTTP_PRIORITY               2                               /**< \brief Priority for HTTP task. */
#define HTTP_STACK_SIZE             512                             /**< \brief Stack size for HTTP task. */
#define SNTP_PRIORITY               2                               /**< \brief Priority for SNTP client task. */
#define SNTP_STACK_SIZE             256                             /**< \brief Stack size for SNTP client task. */
#define UPDATE_MOTOR_PRIORITY       2                               /**< \brief Priority for update motor task. */
#define UPDATE_MOTOR_STACK_SIZE     512                             /**< \brief Stack size for update motor task. */
#define SEND_TELEMETRY_PRIORITY     2                               /**< \brief Priority for send telemetry task. */
#define SEND_TELEMETRY_STACK_SIZE   512                             /**< \brief Stack size for send telemetry task. */
#define INIT_DELAY_MS               500                             /**< \brief Period for init in miliseconds. */
#define INIT_DELAY                  pdMS_TO_TICKS(INIT_DELAY_MS)    /**< \brief Period for init in ticks. */
#define SNTP_SERVERS                "170.210.222.10"                /**< \brief Argentinian NTP server IP. */


/* global variable: between 0 and 1 */
uint8_t SYSTEM_LOG_LEVEL = LOG_INFO;
uint16_t encoder_init_value;

/** \brief Mutex to avoid reading the motor value while is being written. */
SemaphoreHandle_t xMutex_motor_data;

TaskHandle_t xHandle1 = NULL;
TaskHandle_t xHandle2 = NULL;
TaskHandle_t xHandle3 = NULL;
TaskHandle_t xHandle4 = NULL;

/**
 * \brief   Called when websocket frame is received.
 * \warning is executed on TCP thread and should return as soon
 *          as possible
 */
void websocket_cb(struct tcp_pcb *pcb, uint8_t *data, u16_t data_len, uint8_t mode) {
    log_trace("received ws stream callback, someone is talking");
    retval_t rv = telemetry_callback_handler(pcb, data, data_len, mode);
    if (rv == RV_OK)
        log_trace("Ws stream callback handled");
    else
        log_error("Ws stream callback exited with error status: %d", rv);
}

/**
 * \brief   Called when new websocket is opened.
 *          When ws://$HOST_IP/stream is hit, it will
 *          create the tasks to receive and send telemetry.
 */
void websocket_open_cb(struct tcp_pcb *pcb, const char *uri) {
    if (!strcmp(uri, "/stream")) {
        log_info("Request for websocket stream");
        BaseType_t xReturned;
        xReturned = xTaskCreate(&send_telemetry_task,
                                "send_telemetry",
                                SEND_TELEMETRY_STACK_SIZE,
                                (void *) pcb,
                                SEND_TELEMETRY_PRIORITY,
                                &xHandle2);
        if (xReturned == pdPASS) {
            log_trace("Task for send telemetry is created");
        } else {
            log_error("Could not allocate memory for send telemetry task");
        }
        xReturned = xTaskCreate(&update_motor_task,
                                "update_motor",
                                UPDATE_MOTOR_STACK_SIZE,
                                (void *) pcb,
                                UPDATE_MOTOR_PRIORITY,
                                &xHandle3);
        if (xReturned == pdPASS) {
            log_trace("Task for update motor is created");
        } else {
            log_error("Could not allocate memory for update motor task");
        }
    }
}

/**
 * \brief   HTTP server task.
 *          sets mutexes, maps HTTP uris, sets handlers
 *          and callbacks (SSI, CGI and websockets)
 * \param   *pvParameters: the TCP socket
 */
void httpd_task(void *pvParameters) {
    /* initialize mutexes for database interaction */
    xMutex_motor_data = xSemaphoreCreateMutex();

    tCGI pCGIs[] = {
        {"/logger/level", (tCGIHandler) logger_level_cgi_handler},
        {"/motor/state", (tCGIHandler) motor_state_cgi_handler},
        {"/motor/rotation", (tCGIHandler) motor_rotation_cgi_handler},
        {"/encoder/value", (tCGIHandler) encoder_value_cgi_handler},
        {"/telemetry/period", (tCGIHandler) telemetry_period_cgi_handler},
    };

    /**
     * \note On SSI tags.
     *       Only use one SSI, the handler will just replace
     *       the tag with the string from a pointer+len. The
     *       function set_ssi_response() should be called
     *       before invoking the SSI enabled file. */
    const char *pcConfigSSITags[] = {
        "response",
    };

    /* register handlers and start the server */
    http_set_cgi_handlers(pCGIs, sizeof (pCGIs) / sizeof (pCGIs[0]));
    http_set_ssi_handler((tSSIHandler) ssi_handler, pcConfigSSITags,
            sizeof (pcConfigSSITags) / sizeof (pcConfigSSITags[0]));

    /* register handlers and start the server */
    websocket_register_callbacks((tWsOpenHandler) websocket_open_cb, (tWsHandler) websocket_cb);
    httpd_init();
    while (true) {
        log_debug("HTTP task executing ...");
        vTaskDelay(HTTP_DELAY);
    }
}


void sntp_task(void *pvParameters) {
    const char *servers[] = {SNTP_SERVERS};

    /* Wait until we have joined AP and are assigned an IP */
    while (sdk_wifi_station_get_connect_status() != STATION_GOT_IP) {
       vTaskDelay(INIT_DELAY);
       log_trace("Waiting for a stable connection ...");
    }

    /* SNTP will request an update each 30 minutes */
    sntp_set_update_delay(30*60000);

    /* Set GMT-3 zone, daylight savings off */
    const struct timezone tz = {0, 0};

    /* SNTP initialization */
    LOCK_TCPIP_CORE();
    sntp_initialize(&tz);
    UNLOCK_TCPIP_CORE();

    /* Servers configuration */
    sntp_set_servers(servers, sizeof(servers) / sizeof(char*));

    /* Delete task */
    log_trace("disconected, delete task");
    vTaskDelete(xHandle4);
}


/**
 * \brief Program entrypoint.
 */
void user_init(void) {
    uart_set_baud(DEFAULT_UART_NUMBER, DEFAULT_BAUD_RATE);
    log_set_level(SYSTEM_LOG_LEVEL);
    log_info("SDK version:%s ", sdk_system_get_sdk_version());

    struct sdk_station_config config = {
        .ssid = WIFI_SSID,
        .password = WIFI_PASS,
    };

    struct ip_info station_ip;

    /* fix IP address, comment to be dynamic  */
    IP4_ADDR(&station_ip.ip, 192, 168, 0, 15);
    IP4_ADDR(&station_ip.gw, 192, 168, 0, 1);
    IP4_ADDR(&station_ip.netmask, 255, 255, 0, 0);
    sdk_wifi_station_dhcpc_stop();

    /* required to call wifi_set_opmode before station_set_config */
    sdk_wifi_set_opmode(STATION_MODE);
    log_trace("Static ip set status : %d", sdk_wifi_set_ip_info(STATION_IF, &station_ip));
    sdk_wifi_station_set_config(&config);
    sdk_wifi_station_connect();

    vTaskDelay(INIT_DELAY);

    /* initialize tasks */
    BaseType_t xReturned;
    xReturned = xTaskCreate(&httpd_task,
                            "httpd_task",
                            HTTP_STACK_SIZE,
                            NULL,
                            tskIDLE_PRIORITY+HTTP_PRIORITY,
                            &xHandle1);
    if (xReturned == pdPASS) {
        log_trace("Task for http is created");
    } else {
        log_error("Could not allocate memory for httpd task");
    }
    xReturned = xTaskCreate(&sntp_task,
                            "sntp_client",
                            SNTP_STACK_SIZE,
                            NULL,
                            SNTP_PRIORITY,
                            &xHandle4);
    if (xReturned == pdPASS) {
        log_trace("Task for sntp client is created");
    } else {
        log_error("Could not allocate memory for sntp client task");
    }
}
